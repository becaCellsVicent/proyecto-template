//======Creación de boton 1
class MiBoton extends HTMLButtonElement{
    constructor(){
        super();
        this.addEventListener('click', function(e){
            buscar();
        });
    }
}
customElements.define('mi-boton-extendido', MiBoton, {extends:'button'});

//=========Creacion de boton 2
class MiBoton2 extends HTMLButtonElement{
    constructor(){
        super();
        this.addEventListener('click', function(e){
            borrar();
        });
    }
}
customElements.define('mi-boton-extendido-2', MiBoton2, {extends:'button'});

//=========Crear barra de busqeuda/////
//elemento donde se van a insertar los nuevos elementos
class BarraDeNavegacion extends HTMLElement{
    constructor(){
        const tpl = document.querySelector('#template1');
        const tplInst = tpl.content.cloneNode(true);
        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(tplInst);
    }
}
customElements.define('barra-de-busqueda', BarraDeNavegacion);

//=========Crear tabla de salida/////
//elemento donde se van a visualizar los datos
class MiTabla extends HTMLElement{
    constructor(){
        const tpl = document.querySelector('#template2');
        const tplInst = tpl.content.cloneNode(true);
        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(tplInst);
    }
}
customElements.define('mi-tabla', MiTabla);

//=======método buscar ==================
function buscar(){
    var elem = document.getElementById('barra-navegacion').shadowRoot;
    const valor_de_busqueda = elem.childNodes[1].value;
    var url = "https://jsonplaceholder.typicode.com/posts";
    fetch(url)
    .then( response =>{
        return response.json();
    })
    .then(data =>{
        mostrardatos(valor_de_busqueda, data);
    })
    .catch( err =>{
        console.log(err);
    });
}
////===============mostrar datos==================
 function mostrardatos(valor_buscado, objeto){
    for(var i = 0; i <= objeto.length; i++){

        userID = objeto[i]['userId'];
        titulo = objeto[i]['title'];
        id = objeto[i]['id'];
        body = objeto[i]['body'];

        if(valor_buscado == id){
            insertar(titulo, id, body);
        } else{
            
        }
 }
}
//===========insertar datos en tabla
function insertar(titulo, id, body){
    var tabla = document.getElementById('mi-tabla').shadowRoot;
    var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].cells[0];
    columnaId.textContent = id;
    var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].cells[1];
    columnaId.textContent = titulo;
    var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].cells[2];
    columnaId.textContent = body;
}
//===========borrar
function borrar(){
    var tabla = document.getElementById('mi-tabla').shadowRoot;
    var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].cells[0];
    columnaId.textContent = "";
    var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].cells[1];
    columnaId.textContent = "";
    var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].cells[2];
    columnaId.textContent = "";
}